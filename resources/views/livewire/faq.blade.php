<div>
    <div class="container">
        <div class="row mt-5">
            <div class="col-lg-1">
            </div>
            <div class="col-lg-8 text-start mt-5">
                <p class="text-dark h3 mt-5">
                    <b>1. Frequently Asked Questions?</b>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-1">
            </div>
            <div class="col-lg-8 text-start mt-4">
                <p class="text-dark h5">
                    <b>For some time I thought about a section for this website dedicated<br>
                        to my workflow. Because many people kept asking over the years<br>
                        what I do, how I take my photographs, how I retouch, what gear I<br>
                        use and so on. But then I saw a presentation by Studio Pandan.<br>
                        They made a whole presentation out of their FAQ which seemed a<br>
                        great idea. I will add more questions and answers over time. For<br>
                        now, let’s start …</b>
                </p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-1">
            </div>
            <div class="col-lg-8 text-start mt-5">
                <p class="text-dark h3 mt-5">
                    <b>2. Are you employed or do you work as a freelancer?</b>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-1">
            </div>
            <div class="col-lg-8 text-start mt-4">
                <p class="text-dark h5">
                    <b>In 2013 I decided to work as a full-time photographer<br>
                        and retoucher on a freelance base. It’s not a hobby. It’s a job. And I really like what<br>
                        I am allowed to do every day.</b>
                </p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-1">
            </div>
            <div class="col-lg-8 text-start mt-5">
                <p class="text-dark h3 mt-5">
                    <b>3. Photographer is not a real job‽</b>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-1">
            </div>
            <div class="col-lg-8 text-start mt-4">
                <p class="text-dark h5">
                    <b>“Photography? That's easy. All you have to do is push the button.”,<br>
                        This is a
                        usual response when people learn what I do for a living. In<br>
                        addition I am often called an artist ...</b>
                </p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-1">
            </div>
            <div class="col-lg-8 text-start mt-5">
                <p class="text-dark h3 mt-5">
                    <b>4. Is photography an art form?</b>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-1">
            </div>
            <div class="col-lg-8 text-start mt-4">
                <p class="text-dark h5">
                    <b>It depends on how you see yourself and your work. In my opinion I<br>
                        am not an artist. What I offer is a service. Normally when someone<br>
                        tells me about a project they already know what they want but<br>
                        usually they don't know how to get it. Together with my clients I try<br>
                        to solve those photographic problems. Whether it is a whole photo<br>
                        session or retouching of already existing photographs.</b>
                </p>
                <p class="text-dark h5 mt-4">
                    <b>I don’t work in a vacuum. My daily work is characterised by<br>
                    constraints and limits. Limits like time, budget, and resources. If I<br>
                    were an artist, I wouldn't have to pay attention to that. Then I would<br>
                    take pink and blue photos with my Polaroid every day. No matter if<br>
                    there is a demand for it.</b>
                </p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-1">
            </div>
            <div class="col-lg-8 text-start mt-5">
                <p class="text-dark h3 mt-5">
                    <b>5. Are you interested in analog film photography?</b>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-1">
            </div>
            <div class="col-lg-8 text-start mt-4">
                <p class="text-dark h5">
                    <b>Yes, partly. I took and developed analog film myself but nowadays I<br>
                        am 100% focused on digital imagery. Despite that from time to time<br>
                        I play around with Polaroids. But this is just a hobby.</b>
                </p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-1">
            </div>
            <div class="col-lg-8 text-start mt-5">
                <p class="text-dark h3 mt-5">
                    <b>6. AWhat equipment do you use lately?</b>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-1">
            </div>
            <div class="col-lg-8 text-start mt-4">
                <p class="text-dark h5">
                    <b>Sony Alpha 7R IV. Sony Alpha 9 II · mounted on Novoflex precision<br>
                        gear and Benro geared heads</b>
                </p>
                <p class="text-dark h5 mt-4">
                    <b>Schneider-Kreuznach Apo-Digitar ƒ4.5/90 · Sony FE ƒ1.4/24 GM ·<br>
                        Zeiss Batis 2/40 CF · Sony FE ƒ1.4/85 GM · Sony FE ƒ2.8/90 Macro<br>
                        G OSS · Sony FE ƒ2.8/70–200 GM OSS II · Venus Optics ƒ14/24<br>
                        Macro 2× Probe · Venus Optics ƒ2.8/25 Ultra Macro 2.5–5×
                    </b>
                </p>
                <p class="text-dark h5 mt-4">
                    <b>Profoto B10 Plus · 2 × Profoto B10 · Profoto A1 · 12 × Spiffy Gear<br>
                        Spekular Lights</b>
                </p>
                <p class="text-dark h5 mt-4">
                    <b>CaptureOne Pro · Adobe Photoshop CC · Wacom Intuos Pro M</b>
                </p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-1">
            </div>
            <div class="col-lg-8 text-start mt-5">
                <p class="text-dark h3 mt-5">
                    <b>7. Why are you showing off with your gear?</b>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-1">
            </div>
            <div class="col-lg-8 text-start mt-4">
                <p class="text-dark h5">
                    <b>It might look and feel that I am showing off with the<br>
                        list of equipment but on the other hand it’s a very ordinary setup. There is<br>
                        no special medium format camera or other fancy stuff. Just the<br>
                        right tools for the job. For some of my clients it's important to know<br>
                        what kind of gear I use. They see that it’s light-weight, mobile, and<br>
                        versatile. This answers many questions in advance.</b>
                </p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-1">
            </div>
            <div class="col-lg-8 text-start mt-5">
                <p class="text-dark h3 mt-5">
                    <b>8. JPG or RAW?</b>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-1">
            </div>
            <div class="col-lg-8 text-start mt-4">
                <p class="text-dark h5">
                    <b>IAlways RAW! Sometimes I save additional JPG. But this is only for<br>
                        time-critical events. For a full explanation of what a RAW file is<br>
                        check out the corresponding Wikipedia article.</b>
                </p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-1">
            </div>
            <div class="col-lg-8 text-start mt-5">
                <p class="text-dark h3 mt-5">
                    <b>9. Do you manipulate your photographs?</b>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-1">
            </div>
            <div class="col-lg-8 text-start mt-4">
                <p class="text-dark h5">
                    <b>Yes, I do. But I would call it enhancements and not manipulations. It<br>
                     is an essential part of my workflow.</b>
                </p>
                <p class="text-dark h5 mt-4">
                    <b>We all have viewing habits. Also there is a difference in looking at<br>
                        someone face to face or at a photograph. The perception is totally<br>
                        different. You see more details, you pay much more attention to<br>
                        flaws, blemeshes, spots, wrinkles, dark under-eyes, you name it. In<br>
                        addition a camera gives its own interpretation of the real world that<br>
                        mostly is not the result you would expect and – more importantly – <br>
                        accept. Retouching helps to correct this.</b>
                </p>
                <p class="text-dark h5 mt-4">
                    <b>Long story short: I match perception and reality.</b>
                </p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-1">
            </div>
            <div class="col-lg-8 text-start mt-5">
                <p class="text-dark h3 mt-5">
                    <b>10. C’mon, a bit of contrast and brightness is all you have to fix, isn’t it?</b>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-1">
            </div>
            <div class="col-lg-8 text-start mt-4">
                <p class="text-dark h5">
                    <b>Very often the photograph that looks completely average is the one<br>
                        I have put a disproportional amount of work into it. Normally a lot of<br>
                        tweaking goes into perfecting my photos. Currently I use Phase<br>
                         One’s Capture One and Adobe Photoshop.</b>
                </p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-1">
            </div>
            <div class="col-lg-8 text-start mt-5">
                <p class="text-dark h3 mt-5">
                    <b>11. Why do you use Photoshop?</b>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-1">
            </div>
            <div class="col-lg-8 text-start mt-4">
                <p class="text-dark h5">
                    <b>Because I earn my money with it. Over the years I invested<br>
                    thousands of hours into training my skills and I got used to it. I<br>
                    customised the whole experience including Photoshop’s shortcuts;<br>
                    don’t underestimate the value of muscle memory here. Plugins and<br>
                    actions speed up everything I do on a daily base.<br>
                    a</b>
                </p>
                <p class="text-dark h5">
                    <b>Of course I am aware of “alternatives” like Affinity Photo. For a lot of<br>
                     people and a lot of easy tasks it’s quite all right. But 30 years of<br>
                    expertise in photo manipulation is hard to match and I have to<br>
                    admit: I ♥ Ps. It’s great. It’s overwhelming, confusing, and at the<br>
                    same time extremely fun to use. The first version I ever started was<br>
                    5.5 and it got me right away. I get kicks out of pushing around<br>
                    pixels in it. Saving a final™ Photoshop document is a very rewarding<br>
                    moment.</b>
                </p>
            </div>
        </div>
    </div>
</div>
