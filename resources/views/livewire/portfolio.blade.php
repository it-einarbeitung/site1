<div>
   <div class="row mt-5">
    <div class="col-lg-2"></div>
       <div class="col-lg-3 mt-5">
           <p class="text-dark h3 mt-5">
              <b>Portraits</b> 
           </p>
   </div>
</div>
    <div class="row mt-5">
        <div class="col-lg-12">
            <div class="gallery js-flickity">
                <div id="carouselExampleDark" class="carousel carousel-dark slide" data-bs-ride="carousel">
                    <div class="carousel-indicators">
                        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="1" aria-label="Slide 2">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="2" aria-label="Slide 3">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="3" class="active" aria-current="true" aria-label="Slide 4">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="4" class="active" aria-current="true" aria-label="Slide 5">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="5" class="active" aria-current="true" aria-label="Slide 6">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="6" class="active" aria-current="true" aria-label="Slide 7">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="7" class="active" aria-current="true" aria-label="Slide 8">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="8" class="active" aria-current="true" aria-label="Slide 9">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="9" class="active" aria-current="true" aria-label="Slide 10">
                        </button>
                    </div>
                    <div class="carousel-inner">
                        <div class="carousel-item active" data-bs-interval="10000">
                            <img src="/images/Portraits/aoi-2018-2048x.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5 class="text-light">
                                   <b>Aoi Yamaguchi</b>
                                </h5>
                            </div>
                        </div>
                        <div class="carousel-item" data-bs-interval="2000">
                            <img src="/images/Portraits/hendrik-weber-2019-2048x.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5 class="text-light">
                                    <b>Hendrik Weber</b>
                                </h5>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="/images/Portraits/hidetaka-yamasaki-2020-2048x.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5 class="text-light">
                                    <b>Hidetaka Yamasaski</b>
                                </h5>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="/images/Portraits/rickgriffith-2018-2048x.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5 class="text-light">
                                    <b>Rick Griffith</b>
                                </h5>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="/images/Portraits/anja-meiners-2019-windows-2048x.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5 class="text-light">
                                    <b>Anja Meiners</b>
                                </h5>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="/images/Portraits/djr-atypi-2018-2048x.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5 class="text-light">
                                    <b>David Jonathan Ross</b>
                                </h5>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="/images/Portraits/chriscampe-2017-2048x.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5 class="text-light">
                                    <b>Chris Campe</b>
                                </h5>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="/images/Portraits/thorsten-hokema-2018-2048x.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5 class="text-light">
                                    <b>Thorsten Hokema</b>
                                </h5>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="/images/Portraits/sarahlincoln-2018-2048x.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5 class="text-light">
                                    <b>Sarah Lincoln</b>
                                </h5>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="/images/Portraits/davidcarson-2019-2048x.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5 class="text-light">
                                    <b>David Carson</b>
                                </h5>
                            </div>
                        </div>
                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true">
                        </span>
                        <span class="visually-hidden">
                            Previous
                        </span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true">
                        </span>
                        <span class="visually-hidden">
                            Next
                        </span>
                    </button>
                </div>
            </div>
        </div>
    </div>
   <div class="row mt-5">
        <div class="col-lg-2"></div>
           <div class="col-lg-3 mt-5">
               <p class="text-dark h3 mt-5">
                  <b>Objects and Campaigns</b> 
               </p>
       </div>
    </div>
    <div class="row mt-5">
        <div class="col-lg-12">
            <div class="gallery js-flickity">
                <div id="carouselExampleDark1" class="carousel carousel-dark slide" data-bs-ride="carousel">
                    <div class="carousel-indicators">
                        <button type="button" data-bs-target="#carouselExampleDark1" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleDark1" data-bs-slide-to="1" aria-label="Slide 2">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleDark1" data-bs-slide-to="2" aria-label="Slide 3">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleDark1" data-bs-slide-to="3" class="active" aria-current="true" aria-label="Slide 4">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleDark1" data-bs-slide-to="4" class="active" aria-current="true" aria-label="Slide 5">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleDark1" data-bs-slide-to="5" class="active" aria-current="true" aria-label="Slide 6">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleDark1" data-bs-slide-to="6" class="active" aria-current="true" aria-label="Slide 7">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleDark1" data-bs-slide-to="7" class="active" aria-current="true" aria-label="Slide 8">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleDark1" data-bs-slide-to="8" class="active" aria-current="true" aria-label="Slide 9">
                        </button>
                    </div>
                    <div class="carousel-inner">
                        <div class="carousel-item active" data-bs-interval="10000">
                            <img src="/images/Objects/bh-110095-2048x.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5 class="text-dark">
                                   <b>Bauhaus-Archiv / Benita Koch-Otte</b>
                                </h5>
                            </div>
                        </div>
                        <div class="carousel-item" data-bs-interval="2000">
                            <img src="/images/Objects/10049-2-bauhaus-archiv-np-web-2048x.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5 class="text-dark">
                                    <b>Bauhaus-Archiv / OFFSET 7 (1926)</b>
                                </h5>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="/images/Objects/77526-2-bauhaus-archiv-np-web-2048x.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5 class="text-dark">
                                    <b>catalogue: “original bauhaus – the centenary exhibition” (2019)</b>
                                </h5>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="/images/Objects/201705101978-hering-vase-matt-2048x.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5 class="text-dark">
                                    <b>Hering Berlin (Porcelain)</b>
                                </h5>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="/images/Objects/greenpeace-2048x.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5 class="text-dark mb-4">
                                    <b>Anja Meiners</b>
                                </h5>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="/images/Objects/vcard-np-2021-2048x.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5 class="text-dark">
                                    <b>vCards</b>
                                </h5>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="/images/Objects/vaay-products-2048-np-2020-2048x.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5 class="text-dark">
                                    <b>VAAY</b>
                                </h5>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="/images/Objects/2021-re-sku-sunnies-2048x.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5 class="text-dark">
                                    <b>re-SKU</b>
                                </h5>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="/images/Objects/volta-press-66-2048x.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5 class="text-dark">
                                    <b>Volta Press – Sixty-Six Deck</b>
                                </h5>
                            </div>
                        </div>
                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleDark1" data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true">
                        </span>
                        <span class="visually-hidden">
                            Previous
                        </span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleDark1" data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true">
                        </span>
                        <span class="visually-hidden">
                            Next
                        </span>
                    </button>
                </div>
            </div>
        </div>
    </div>
   <div class="row mt-5">
        <div class="col-lg-2"></div>
           <div class="col-lg-3 mt-5">
               <p class="text-dark h3 mt-5">
                  <b>Features</b> 
               </p>
       </div>
    </div>
    <div class="row mt-5">
        <div class="col-lg-12">
            <div class="gallery js-flickity">
                <div id="carouselExampleDark2" class="carousel carousel-dark slide" data-bs-ride="carousel">
                    <div class="carousel-indicators">
                        <button type="button" data-bs-target="#carouselExampleDark2" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleDark2" data-bs-slide-to="1" aria-label="Slide 2">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleDark2" data-bs-slide-to="2" aria-label="Slide 3">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleDark2" data-bs-slide-to="3" class="active" aria-current="true" aria-label="Slide 4">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleDark2" data-bs-slide-to="4" class="active" aria-current="true" aria-label="Slide 5">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleDark2" data-bs-slide-to="5" class="active" aria-current="true" aria-label="Slide 6">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleDark2" data-bs-slide-to="6" class="active" aria-current="true" aria-label="Slide 7">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleDark2" data-bs-slide-to="7" class="active" aria-current="true" aria-label="Slide 8">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleDark2" data-bs-slide-to="8" class="active" aria-current="true" aria-label="Slide 9">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleDark2" data-bs-slide-to="9" class="active" aria-current="true" aria-label="Slide 10">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleDark2" data-bs-slide-to="10" class="active" aria-current="true" aria-label="Slide 11">
                        </button>
                    </div>
                    <div class="carousel-inner">
                        <div class="carousel-item active" data-bs-interval="10000">
                            <img src="/images/Features/20191104-171650-gpi-conference-np-2048x.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5 class="text-light">
                                   <b>Christine Mhundwa @ GPI Conference, 2019</b>
                                </h5>
                            </div>
                        </div>
                        <div class="carousel-item" data-bs-interval="2000">
                            <img src="/images/Features/ableton-loop-2015-1-2048x.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5 class="text-light">
                                    <b>Ableton Loop 2015</b>
                                </h5>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="/images/Features/ableton-loop-2015-2-2048x.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5 class="text-light">
                                    <b>Ableton Loop 2015</b>
                                </h5>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="/images/Features/cmber-2019-1-1-2048x.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5 class="text-light">
                                    <b>CreativeMornings/BER · 2019-1</b>
                                </h5>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="/images/Features/cmber-2019-1-2-2048x.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5 class="text-light">
                                    <b>CreativeMornings/BER · 2019-1</b>
                                </h5>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="/images/Features/edch-2018-1-2048x.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5 class="text-light">
                                    <b>EDCH 2018 in Munich</b>
                                </h5>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="/images/Features/edch-2018-6-2048x.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5 class="text-dark">
                                    <b></b>
                                </h5>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="/images/Features/2020-02-13-0001-psl-2048x.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5 class="text-light">
                                    <b>www.peptid.de</b>
                                </h5>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="/images/Features/2020-02-13-0002-psl-2048x.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5 class="text-dark">
                                    <b>www.peptid.de</b>
                                </h5>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="/images/Features/2020-02-13-0003-psl-2048x.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5 class="text-dark">
                                    <b>www.peptid.de</b>
                                </h5>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="/images/Features/edch-2018-4-2048x.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5 class="text-dark">
                                    <b></b>
                                </h5>
                            </div>
                        </div>
                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleDark2" data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true">
                        </span>
                        <span class="visually-hidden">
                            Previous
                        </span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleDark2" data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true">
                        </span>
                        <span class="visually-hidden">
                            Next
                        </span>
                    </button>
                </div>
            </div>
        </div>
    </div>
   <div class="row mt-5">
        <div class="col-lg-2"></div>
           <div class="col-lg-3 mt-5">
               <p class="text-dark h3 mt-5">
                  <b>fStop Images</b> 
               </p>
       </div>
    </div>
    <div class="row mt-5">
        <div class="col-lg-12">
            <div class="gallery js-flickity">
                <div id="carouselExampleDark3" class="carousel carousel-dark slide" data-bs-ride="carousel">
                    <div class="carousel-indicators">
                        <button type="button" data-bs-target="#carouselExampleDark3" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleDark3" data-bs-slide-to="1" aria-label="Slide 2">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleDark3" data-bs-slide-to="2" aria-label="Slide 3">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleDark3" data-bs-slide-to="3" class="active" aria-current="true" aria-label="Slide 4">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleDark3" data-bs-slide-to="4" class="active" aria-current="true" aria-label="Slide 5">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleDark3" data-bs-slide-to="5" class="active" aria-current="true" aria-label="Slide 6">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleDark3" data-bs-slide-to="6" class="active" aria-current="true" aria-label="Slide 7">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleDark3" data-bs-slide-to="7" class="active" aria-current="true" aria-label="Slide 8">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleDark3" data-bs-slide-to="8" class="active" aria-current="true" aria-label="Slide 9">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleDark3" data-bs-slide-to="9" class="active" aria-current="true" aria-label="Slide 10">
                        </button>
                    </div>
                    <div class="carousel-inner">
                        <div class="carousel-item active" data-bs-interval="10000">
                            <img src="/images/fStop/gettyimages-1042678610-2048x2048-2048x.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                
                            </div>
                        </div>
                        <div class="carousel-item" data-bs-interval="2000">
                            <img src="/images/fStop/gettyimages-1094957158-2048x2048-2048x.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="/images/fStop/gettyimages-1158243172-2048x2048-2048x.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="/images/fStop/gettyimages-564718169-2048x2048-2048x.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="/images/fStop/gettyimages-1326412763-2048x2048-2048x.jpg" class="d-block w-100" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="/images/fStop/gettyimages-1326412902-2048x2048-2048x.jpg" class="d-block w-100" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="/images/fStop/gettyimages-1326412773-2048x2048-2048x.jpg" class="d-block w-100" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="/images/fStop/gettyimages-1270218157-2048x2048-2048x.jpg" class="d-block w-100" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="/images/fStop/gettyimages-1172129174-2048x2048-2048x.jpg" class="d-block w-100" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="/images/fStop/gettyimages-769722317-2048x2048-2048x.jpg" class="d-block w-100" alt="...">
                        </div>
                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleDark3" data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true">
                        </span>
                        <span class="visually-hidden">
                            Previous
                        </span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleDark3" data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true">
                        </span>
                        <span class="visually-hidden">
                            Next
                        </span>
                    </button>
                </div>
            </div>
        </div>
    </div>  
</div>
