<div>
    <div class="row">
        <div class="col-lg-12">
            <nav class="navbar navbar-expand-lg navbar-light bg-light  border border-2 fixed-top">
                <div class="container-fluid">
                    <i class="far fa-circle text-danger h1"></i>
                    <a class="navbar-brand text-dark active-danger ms-3" href="#">
                        <b>Norman Posselt</b><br>
                        Photographer. Retoucher.
                    </a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon">
                        </span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link active me-3" aria-current="page" href="#">
                                   <b> Home</b>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link me-3 text-dark " href="#">

                                    <b>Portfolio</b>
                                   
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-dark me-5" href="#">
                                    <b>FAQ</b>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-dark me-3" href="#">
                                    <b>DE</b>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-dark me-3" href="#">
                                    <b>EN</b>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</div>
