<div>
    <div class="container">
        <div class="row mt-5">
            <div class="col-lg-1"></div>
            <div class="col-lg-5 mt-5">
               <p class="text-dark h2">
                    <b>Contact · Imprint · Privacy Policy</b>
               </p>
               <p class="text-dark h5 mt-3">
                    <b> I am currently available for assignments. If you<br>
                        have a project you would like to work with me<br>
                        on, let us get in touch:</b>
               </p>
               <a class="text-danger nav nav-link h4 mt-5" href="#">
                   mail@normanposselt.com<br>
                   Instagram<br>
                    Medium<br>
                    Twitter
               </a>
               <a href="#" class="nav nav-link text-danger h4 mt-4">
                   Getty Images
               </a>
               <a href="#" class="nav nav-link text-danger h4 mt-4">
                   LinkedIn<br>
                   vCard
               </a>
            </div>
            <div class="col-lg-6 mt-5">
                <p class="text-dark h5 mt-5">
                  <b>  I’m responsible for content on this website but<br>
                        not for those on websites I link to.
                    </b>
                </p>
                <p class="text-dark h5 mt-5">
                    <b>Norman Posselt<br>
                        Prinz-Eugen-Str. 16<br>
                        13347 Berlin
                    </b>
                </p>
                <p class="text-dark h5 mt-5">
                    <b>VAT Number: DE313796237</b>
                    <a href="#" class="nav nav-link text-danger h5">
                        <b> Privacy Policy</b>
                    </a>
                </p>
                <p class="text-dark h3 mt-4">
                    <b>Type</b>
                </p>
                <p class="text-dark h5">
                    <b>Fabrikat Normal by Christoph Koeberlin, Inga<br>
                        Plönnigs, Hannes von Döhren</b>
                </p>
                <p class="text-dark h3 mt-4">
                    <b>Icons</b>
                </p>
                <p class="text-dark h5">
                    <b>Camera Icon by Ferdinand Ulrich<br>
                        Super Head Portrait by eBoy</b>
                </p>
                <p class="text-dark h3 mt-4">
                    <b>Code</b>
                </p>
                <p class="text-dark h5">
                    <b>This website was crafted by Frank Rausch in Berlin,<br>
                        Germany. Thank you, mate!</b>
                </p>
                <p class="text-dark h3 mt-4">
                    <b>Copyright</b>
                </p>
                <p class="text-dark h5">
                    <b>The content (texts, pictures, graphics) of this<br>
                    website remains the exclusive copyright<br>
                    property of the creator. No rights are granted<br>
                    unless written contracts are in place. © 2022 ·<br>
                    Norman Posselt · VG Bild-Kunst</b>
                </p>
            </div>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-lg-12 text-center mt-5">
            <i class="far fa-circle text-danger h1"></i>
        </div>
    </div>
</div>
