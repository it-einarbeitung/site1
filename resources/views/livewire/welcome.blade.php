<div>
    <div class="container mt-5">
        <div class="d-none d-lg-block">
            <div class="row mt-5">
                <div class="col-lg-8 text-center mt-5">
                    <p class="text-dark h1 mt-5">
                        <b>“My photographs are answers,<br> not questions.”</b>
                    </p>
                </div>
            </div>
        </div>
        <div class="d-block d-lg-none">
            <div class="row mt-5">
                <div class="col-lg-8 text-center mt-5">
                    <p class="text-dark h1 mt-5">
                        <b>“No compromises on gear.”</b>
                    </p>
                </div>
            </div>
        </div>
       <!--  <div class="d-none d-lg-block"> -->
            <div class="row mt-5">
                <div class="col-lg-2"></div>
                <div class="col-lg-5 text-start mt-5">
                    <p class="text-dark h5">
                        <b>Hello! My name is Norman. I am a commercial<br>
                        photographer and retoucher based in Berlin and<br>
                        working worldwide.</b>
                    </p>
                    <p class="text-dark h5 mt-4">
                        <b>I am specialised in corporate and archival<br>
                            photography – objects of all kinds. This ranges<br>
                            from consumer goods and prototypes to<br>
                            historical artefacts in museums and archives.</b>
                        </p>
                </div>
                <div class="col-lg-5 text-start mt-5">
                    <p class="text-dark h5">
                        <b>I also have a soft spot for portraits and special<br>
                            events like conferences. In any case it is all<br>
                            about taking visually striking, professional<br>
                            photographs right on spot that support my<br>
                            clients and their goals.</b>
                    </p>
                    <p class="text-dark h5 mt-4">
                        <b>In short: I don’t take photographs that stand out<br>
                            but stand apart.</b>
                        </p>
                </div>
            </div>
        <!-- </div> -->
    </div>
    <div class="row mt-5">
        <div class="col-lg-12">
            <div class="gallery js-flickity">
                <div id="carouselExampleDark" class="carousel carousel-dark slide" data-bs-ride="carousel">
                    <div class="carousel-indicators">
                        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="1" aria-label="Slide 2">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="2" aria-label="Slide 3">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="3" class="active" aria-current="true" aria-label="Slide 4">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="4" class="active" aria-current="true" aria-label="Slide 5">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="5" class="active" aria-current="true" aria-label="Slide 6">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="6" class="active" aria-current="true" aria-label="Slide 7">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="7" class="active" aria-current="true" aria-label="Slide 8">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="8" class="active" aria-current="true" aria-label="Slide 9">
                        </button>
                        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="9" class="active" aria-current="true" aria-label="Slide 10">
                        </button>
                    </div>
                    <div class="carousel-inner">
                        <div class="carousel-item active" data-bs-interval="10000">
                            <img src="/images/2021np-2048x.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5 class="text-light">
                                   Ivo Gabrowitsch
                                </h5>
                            </div>
                        </div>
                        <div class="carousel-item" data-bs-interval="2000">
                            <img src="/images/christoph-rauscher-2020-2048x.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5 class="text-light">
                                    Christoph Rauscher
                                </h5>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="/images/cmber-2020-2048x.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5 class="text-light">
                                    2020 Team of CreativeMornings/Berlin
                                </h5>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="/images/eva-lotta-2020-0-2048x.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5 class="text-light">
                                    Eva-Lotta Lamm
                                </h5>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="/images/fabian-friede-2020-2048x.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5 class="text-light">
                                    Fabian Friede / VAAY
                                </h5>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="/images/julieheumueller-2019-2048x.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5 class="text-light">
                                    Julie Heumüller
                                </h5>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="/images/wilfrid-wood-2019-2048x.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5 class="text-light">
                                    Wilfrid Wood @btconf Berlin 2019
                                </h5>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="/images/wikipedia-2048x.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5 class="text-light">
                                    Portraits @Wikipedia via CC BY-SA 4.0
                                </h5>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="/images/sonjaknecht-2020-2048x.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5 class="text-light">
                                    Sonja Knecht
                                </h5>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="/images/ollimeier-2021-2048x.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5 class="text-light">
                                    Olli Meier
                                </h5>
                            </div>
                        </div>
                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true">
                        </span>
                        <span class="visually-hidden">
                            Previous
                        </span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true">
                        </span>
                        <span class="visually-hidden">
                            Next
                        </span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- external css: -->
    <!-- <div class="row mt-5">
        <div class="col-lg-12">
            <div class="gallery js-flickity"
                data-flickity-options='{ "wrapAround": true }'>

                <div class="gallery-cell">

                        <img src="/images/cmber-2020-2048x.jpg" >
                </div>
                <div class="gallery-cell">
                     <img src="/images/cmber-2020-2048x.jpg">
                </div>
                <div class="gallery-cell">
                     <img src="/images/cmber-2020-2048x.jpg" >
                </div>
                <div class="gallery-cell">
                     <img src="/images/cmber-2020-2048x.jpg" >
                </div>
                <div class="gallery-cell">
                     <img src="/images/cmber-2020-2048x.jpg" >
                </div>
            </div>
        </div>
    </div> -->
    <div class="container">
        <div class="d-none d-lg-block">
            <div class="row mt-5">
                <div class="col-lg-6 text-center mt-5">
                    <p class="text-dark h1">
                        <b>Focus</b>
                    </p>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-lg-6 text-center">
                    <p class="text-dark h4 me-5">
                        <b>Photography</b>
                    </p>
                </div>
                <div class="col-lg-1"></div>
                <div class="col-lg-4 text-start">
                    <p class="text-dark h4">
                        <b>Retouching</b>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-2"></div>
                <div class="col-lg-4 text-start">
                    <p class="text-dark  h5">
                        -  Product Photography (w/ an accuracy of 0.2 µ)
                    </p>
                    <p class="text-dark  h5">
                        -   Archival Photography – Digitization according<br> 
                            to the standards of the Stiftung
                            Preußischer<br> 
                            Kulturbesitz; w/ up to 240.8 megapixels
                    </p>
                    <p class="text-dark h5">
                        -  Film Scanning [35 mm film (strip, mounted),<br>
                            120 (medium) format film w/ up to 9600 dpi]
                    </p>
                    <p class="text-dark h5">
                        -  Portrait and Conference Photography
                    </p>
                </div>
                <div class="col-lg-1"></div>
                <div class="col-lg-4 text-start">
                    <p class="text-dark h5">
                        -  high quality, ready for different use-cases
                    </p>
                    <p class="text-dark h5">
                        - Retouching of your Photographs
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="d-block d-lg-none">
            <div class="row mt-5">
                <div class="col-lg-12 text-start mt-5">
                    <p class="text-dark h1">
                        <b>Focus</b>
                    </p>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-lg-12 text-start">
                    <p class="text-dark h4 ">
                        <b>Photography</b>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 text-start">
                    <p class="text-dark  h5">
                        -  Product Photography (w/ an accuracy of 0.2 µ)
                    </p>
                    <p class="text-dark  h5">
                        -   Archival Photography – Digitization according<br> 
                            to the standards of the Stiftung
                            Preußischer<br> 
                            Kulturbesitz; w/ up to 240.8 megapixels
                    </p>
                    <p class="text-dark h5">
                        -  Film Scanning [35 mm film (strip, mounted),<br>
                            120 (medium) format film w/ up to 9600 dpi]
                    </p>
                    <p class="text-dark h5">
                        -  Portrait and Conference Photography
                    </p>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-lg-12 text-start">
                    <p class="text-dark h4 ">
                        <b>Retouching</b>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 text-start">
                    <p class="text-dark  h5">
                        -  high quality, ready for different use-cases
                    </p>
                    <p class="text-dark  h5">
                        -  Retouching of your Photographs
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="d-none d-lg-block">
        <div class="row mt-5">
            <div class="col-lg-3"></div>
            <div class="col-lg-3 text-start mt-5">
                <p class="text-dark h1">
                    <b>Approach</b>
                </p>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-lg-3"></div>
            <div class="col-lg-4 text-start">
                <p class="text-dark h5">
                   <b>My photographs are meticulously made. They are<br>
                    defined by a fresh and natural look. No matter if<br>
                    bright and vivid colors or a deep and rich<br>
                    monochromatic spectrum.</b>
                </p>
                <p class="text-dark h5 mt-5">
                   <b>It all starts with the right equipment. With a mix<br>
                    of prime and zoom lenses I get the very best<br>
                    results for all things I am focused on. Whether it<br>
                    is the shallow depth of a blurry bokeh in a portrait, a<br>
                    razor sharp detail or the ultra-wide<br>
                    view of an event space I rely on my gear. Like a<br>
                    carpenter needs his plane, chisel, and a hammer.</b>
                </p>
                <p class="text-dark h5 mt-5">
                  <b>  The right environment in combination with a<br>
                    sophisticated light setup is an essential part, too<br>
                    – natural light, flashes, huge soft boxes, grids, and gels.</b>
                </p>
                <p class="text-dark h5 mt-5">
                 <b>My highly trained skills and techniques of digital<br>
                    retouching enable a pristine appearance. I<br>
                    tastefully eliminate blemishes and emphasize<br>
                    details, correct lights / shadows, and fine tune<br>
                    perspectives as well as geometry. It culminates<br>
                    in nuanced sharpening and color grading.</b>
                </p>
            </div>
            <div class="col-lg-5 text-start">
                <img src="/images/np-gk-2018-2048x.jpg" class="img-fluid">
                <p class="text-dark h5">
                    Norman Posselt, photographed by Gerhard Kassner in 2018
                </p>
            </div>
        </div>
    </div>
    <div class="d-block d-lg-none">
        <div class="row mt-5">
            <div class="col-lg-3"></div>
            <div class="col-lg-3 text-start mt-5">
                <p class="text-dark h1">
                    <b>Approach</b>
                </p>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-lg-12 text-start">
                <p class="text-dark h5">
                   <b>My photographs are meticulously made. They are<br>
                    defined by a fresh and natural look. No matter if<br>
                    bright and vivid colors or a deep and rich<br>
                    monochromatic spectrum.</b>
                </p>
                <p class="text-dark h5 mt-5">
                   <b>It all starts with the right equipment. With a mix<br>
                    of prime and zoom lenses I get the very best<br>
                    results for all things I am focused on. Whether it<br>
                    is the shallow depth of a blurry bokeh in a portrait, a<br>
                    razor sharp detail or the ultra-wide<br>
                    view of an event space I rely on my gear. Like a<br>
                    carpenter needs his plane, chisel, and a hammer.</b>
                </p>
                <p class="text-dark h5 mt-5">
                  <b>  The right environment in combination with a<br>
                    sophisticated light setup is an essential part, too<br>
                    – natural light, flashes, huge soft boxes, grids, and gels.</b>
                </p>
                <p class="text-dark h5 mt-5">
                 <b>My highly trained skills and techniques of digital<br>
                    retouching enable a pristine appearance. I<br>
                    tastefully eliminate blemishes and emphasize<br>
                    details, correct lights / shadows, and fine tune<br>
                    perspectives as well as geometry. It culminates<br>
                    in nuanced sharpening and color grading.</b>
                </p>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-lg-12 text-start">
                <img src="/images/np-gk-2018-2048x.jpg" class="img-fluid">
                <p class="text-dark h5">
                    Norman Posselt, photographed by Gerhard Kassner in 2018
                </p>
            </div>
        </div>
    </div>
    <div class="d-none d-lg-block">
        <div class="row mt-5">
            <div class="col-lg-2"></div>
            <div class="col-lg-3 text-start">
                <p class="text-dark h2">
                    <b>Retouching</b>
                </p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-6">
                <img src="/images/20211122t141007-2048-np.jpg" class="img-fluid">
            </div>
            <div class="col-lg-6 text-start">
                <p class="text-dark h5">
                  <b>Why retouching at all? Quick answer: we all have<br>
                    viewing habits. Also there is a difference in<br>
                    looking at someone face to face or at a<br>
                    photograph. The perception is totally different.<br>
                    You see more details, you pay much more<br>
                    attention to flaws, blemeshes, spots, wrinkles,<br>
                    dark under-eyes, you name it. In addition a<br>
                    camera gives its own interpretation of the real<br>
                    world that mostly is not the result you would<br>
                    expect and – more importantly – accept.<br>
                    Retouching helps to correct this.</b>
                </p>
                <p class="text-dark h5 mt-5">
                   <b>The basic idea is to get the very best out of my<br>
                    images that caters my clients’ taste and fits<br>
                    their needs.</b>
                </p>
                <p class="text-dark h5 mt-5">
                   <b> Regarding products people expect certain<br>
                    things: e.g. overall sharpness, no distortion, a<br>
                    deep depth of field, and color accuracy to name<br>
                    a few. My clients want to show their products <br>
                    and objects the best way possible. To get there<br> 
                    Photoshop is an essential part. By combining<br>
                    different photographs of the same object – each<br>
                    one highlighting a different spot – I try create<br>
                    images that show products that have the detail<br>
                    and accuracy of a rendering but at the same<br>
                    time the warmth of real photograph.</b>
                </p>
            </div>
        </div>
    </div>
    <div class="d-block d-lg-none">
        <div class="row mt-5">
            <div class="col-lg-2"></div>
            <div class="col-lg-3 text-start">
                <p class="text-dark h2">
                    <b>Retouching</b>
                </p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-6">
                <img src="/images/20211122t141007-2048-np.jpg" class="img-fluid">
            </div>
            <div class="col-lg-6 text-start mt-5">
                <p class="text-dark h5">
                  <b>Why retouching at all? Quick answer: we all have<br>
                    viewing habits. Also there is a difference in<br>
                    looking at someone face to face or at a<br>
                    photograph. The perception is totally different.<br>
                    You see more details, you pay much more<br>
                    attention to flaws, blemeshes, spots, wrinkles,<br>
                    dark under-eyes, you name it. In addition a<br>
                    camera gives its own interpretation of the real<br>
                    world that mostly is not the result you would<br>
                    expect and – more importantly – accept.<br>
                    Retouching helps to correct this.</b>
                </p>
                <p class="text-dark h5 mt-5">
                   <b>The basic idea is to get the very best out of my<br>
                    images that caters my clients’ taste and fits<br>
                    their needs.</b>
                </p>
                <p class="text-dark h5 mt-5">
                   <b> Regarding products people expect certain<br>
                    things: e.g. overall sharpness, no distortion, a<br>
                    deep depth of field, and color accuracy to name<br>
                    a few. My clients want to show their products <br>
                    and objects the best way possible. To get there<br> 
                    Photoshop is an essential part. By combining<br>
                    different photographs of the same object – each<br>
                    one highlighting a different spot – I try create<br>
                    images that show products that have the detail<br>
                    and accuracy of a rendering but at the same<br>
                    time the warmth of real photograph.</b>
                </p>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row mt-5">
            <div class="col-lg-1"></div>
            <div class="col-lg-5 mt-5">
               <p class="text-dark h2">
                    <b>Notable Clients</b>
               </p>
               <p class="text-dark h5 mt-3">
                  <b> I am in the fortunate position of being able to<br>
                    say that I have great clients. The majority come<br>
                    from the design, media scene, and consumer<br>
                    goods sector. What is important to me in any<br>
                    case is a trusting and honest relationship with<br>
                    each other.</b>
               </p>
            </div>
            <div class="col-lg-6 mt-5">
                <p class="text-dark h5 mt-5">
                  <b>  Ableton · Adobe · Akademie der Künste, Berlin ·<br>
                    ATypI · Bauhaus-Archiv · Global Perspective<br>
                    Initiative · Beyond Tellerrand · Die<br>
                    Bundesregierung · diesdas.digital · Heidelberger<br>
                    Druckmaschinen · Helios · MAGAZIN · Monotype ·<br>
                    PAGE · Pommersches Landesmuseum · PPE<br>
                    Germany · TYPO Berlin  /  TYPO Labs · VAAY  /<br>
                    Sanity Group · VDI  /  VDE Innovation + Technik ·<br>
                    Wall / JCDecaux</b>
                </p>
            </div>
        </div>
    </div>
</div>
